import {Injectable, EventEmitter} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Articles} from "./models/Articles";

@Injectable()
export class NewsAPIService {

    public mainURL = 'https://newsapi.org/v1/articles';


    constructor(private http: Http) {
    }

    getNewsHeadlines(source: String, apiKey: String = 'fa39d1ebb10b4eed9ca78607d93a10d5', sortBy = 'latest'): Observable<Articles> {
        const parsedURL = this.mainURL + '?source=' + source + '&apiKey=' + apiKey + '&sortBy=' + sortBy;

        return this.http.get(parsedURL)
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

}
