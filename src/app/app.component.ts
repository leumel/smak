import {Component, ViewChild} from "@angular/core";
import {NewsAPIService} from "./news-api.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers : [NewsAPIService]
})


export class AppComponent{

  public headlines:Array<string> = [];

  @ViewChild('btnMenu') menu;
  @ViewChild('divMenu') divMenu;
  @ViewChild('imgClose') imgClose;
  @ViewChild('holderClass') holderClass;
  @ViewChild('inputTopic') inputTopic;

  articles : any;

  name ='lem';
  keyString = '';

  visible: Boolean = false;

  initial  = true
  shouldShowTypeAhead = false;

  hasChosen = false


  constructor(private _newsAPIService: NewsAPIService) {

  }

  changed(e){
    if(this.keyString.trim().length !=0){
      this.shouldShowTypeAhead = true
      this.getArticles()
    }
    else{
      this.shouldShowTypeAhead = false
      this.hasChosen = false
    }
  }

  onHeadlineSelect(topic){
    console.log("12")
    this.shouldShowTypeAhead = false
    this.hasChosen = true
    this.keyString = topic
  }


  showMenu() { // click handler
    console.log('print' + this.visible)
    this.visible = true;
  }

  hideMenu() {
    console.log('print2 ' + this.visible)
    this.visible = false;
  }

  getArticles(){
      this._newsAPIService.getNewsHeadlines("the-next-web").subscribe(
          articles => {
            this.articles = JSON.parse(JSON.stringify(articles));
            this.headlines = []
            for (let article of this.articles.articles){
              if (article.title.toLowerCase().indexOf(this.keyString.toLowerCase()) != -1){
                this.headlines.push(article.title)
              }
            }
            this.shouldShowTypeAhead = this.headlines.length > 0 && this.shouldShowTypeAhead;
          }
      )
  }
}
