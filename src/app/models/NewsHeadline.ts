/**
 * Created by lemuel on 7/22/17.
 */
export class NewsHeadline {
    constructor(
        source: String,
        author: String,
        description: String,
        title: String,
        url: String,
        urlToImage: String
    ) {}
}
