/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NewsAPIService } from './news-api.service';

describe('NewsAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsAPIService]
    });
  });

  it('should ...', inject([NewsAPIService], (service: NewsAPIService) => {
    expect(service).toBeTruthy();
  }));
});
